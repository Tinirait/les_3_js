	  var apples = 20, 
	   strawberry = 20, 
	   apricot = 20, 
	   flour = 20,
	   milk = 20, 
	   eggs = 50;
	  console.log('Печем яблочный пирог пирог');
	  function apple_pirogok(apples,flour,milk,eggs){
	  	for(var applePie = 1; applePie <= 10; applePie++){
	  		if (apples - 3 < 0) {
	  			console.log('Кончились яблоки! Купили еще 10 кг')
	  			apples += 10;
	  		} else if (flour - 2 < 0) {
	  			console.log('Кончилась мука! Купили еще 10 кг')
	  			flour +=10;
	  		} else if (milk - 1 < 0) {
	  			console.log('Кончилось молоко! Купили еще 10 литров')
	  			milk +=10;
	  		} else if (eggs - 3 < 0) {
	  			console.log('Кончились яйца! Купили еще 10 штук')
	  			eggs+=10;
	  		} else {
	  			var apples = apples - 3,
	  			 flour = flour - 2,
	  			 milk = milk - 1,
	  			 eggs = eggs - 3;
	  		}
	  		console.log('Яблочный пирог ' +applePie+ 'готов!');
	  	}
	  }
	  console.log(apple_pirogok(apples,flour,milk,eggs));

	  console.log('Печем клубничный пирог');
	  function strawberry_pirogok(strawberry,flour,milk,eggs){
	  	for(var strawberryPie = 1; strawberryPie <=10; strawberryPie++){
	  		if (strawberry - 5 < 0) {
	  			console.log('Кончилась клубника! Купили еще 10 кг')
	  			strawberry = strawberry + 10;
	  		} else if (flour - 2 < 0) {
	  			console.log('Кончилась мука! Купили еще 10 кг')
	  			flour += 10;
	  		} else if (milk - 1 < 0) {
	  			console.log('Кончилось молоко! Купили еще 10 литров')
	  			milk +=10;
	  		} else if (eggs - 3 < 0) {
	  			console.log('Кончились яйца! Купили еще 10 штук')
	  			eggs +=10;
	  		} else {
	  			var strawberry = strawberry - 5,
	  		  flour = flour - 1,
	  			milk = milk - 2,
	  			eggs = eggs - 4;
	  			console.log('Клубничный пирог '+strawberryPie+' готов!');
	  		}
	  	}
	  }
	  console.log(strawberry_pirogok(strawberry,flour,milk,eggs));

	  console.log('Печем абрикосовые пироги');
	  function apricot_pirogok(apricot,flour,milk,eggs){
	  	for(var apricotPie = 1; apricotPie <=10; apricotPie++){
	  		if (apricot - 2 < 0) {
	  			console.log('Кончились абрикосы! Купили еще 10 кг')
	  			apricot +=10;
	  		} else if (flour - 2 < 0) {
	  			console.log('Кончилась мука! Купили еще 10 кг')
	  			flour +=10;
	  		} else if (milk - 1 < 0) {
	  			console.log('Кончилось молоко! Купили еще 10 литров')
	  			milk +=10;
	  		} else if (eggs - 3 < 0) {
	  			console.log('Кончились яйца! Купили еще 10 штук')
	  			eggs +=10;
	  		} else {
	  			var apricot = apricot - 2,
	  			 flour = flour - 3,
	  			 milk = milk - 2,
	  			 eggs = eggs - 2;
	  			console.log('Абрикосовый пирог '+apricotPie+' готов!');
	  		}
	  	}
	  }
	  console.log(apricot_pirogok(apricot,flour,milk,eggs));

	  console.log('Задание-2');
	  //При помощи циклов
	  function sumTo(n){
	  	var summa = 0;
	  	for(var i = 1; i <= n; i++){
	  		summa+=i;
	  	}
	  	return (summa);
	  }
	  console.log('Сумма чисел равна - ' + sumTo(2));
	  //При помощи рекурсии
	  function sumToRecurs(n) {
    if (n == 1) {
      return n;
    }
    else {
      return n + sumToRecurs(n-1);
    }
  }
  console.log('Сумма чисел при помощи рекурсии равна - '+sumToRecurs(2));
	  
